FROM docker.io/alpine as builder

# Postgres Exporter image for OpenShift Origin

ARG PGXPVERS=0.10.0
ENV PGXPURL=https://github.com/prometheus-community/postgres_exporter/releases/download

RUN set -x \
    && if test `uname -m` = aarch64; then \
	ARCH=arm64; \
    elif test `uname -m` = armv7l; then \
	ARCH=armv7; \
    elif test `uname -m` = armv6l; then \
	ARCH=armv6; \
    elif test `uname -m` = armv5l; then \
	ARCH=armv5; \
    else \
	ARCH=amd64; \
    fi \
    && wget -O /tmp/pgxp.tar.gz \
	"$PGXPURL/v$PGXPVERS/postgres_exporter-$PGXPVERS.linux-$ARCH.tar.gz" \
    && tar -C /tmp -xf /tmp/pgxp.tar.gz --strip-components=1 \
    && chmod +x /tmp/postgres_exporter

FROM scratch

ARG PGXPVERS=0.10.0

LABEL io.k8s.description="Postgres Prometheus Exporter Image." \
      io.k8s.display-name="Postgres Prometheus Exporter" \
      io.openshift.expose-services="9187:http" \
      io.openshift.tags="prometheus,exporter,postgres" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-pgexporter" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$PGXPVERS"

COPY --from=builder /tmp/postgres_exporter /postgres_exporter

ENTRYPOINT ["/postgres_exporter"]
USER 1001
