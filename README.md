# k8s Prometheus Postgres Exporter

Based on https://github.com/wrouesnel/postgres_exporter

Build with:

```
$ make build
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name                  |    Description           | Default  |
| :-------------------------------- | ------------------------ | -------- |
|  `DATA_SOURCE_NAME`               | Postgres Connection URL  | undef    |
|  `PG_EXPORTER_WEB_LISTEN_ADDRESS` | Exporter Listen Address  | `:9184`  |
